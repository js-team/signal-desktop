Source: node-signal-desktop
Section: net
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Paolo Greppi <paolo.greppi@libpf.com>
Build-Depends:
 debhelper (>= 10)
 , nodejs (>= 10.15.2~dfsg)
 , yarnpkg
 , git
 , ca-certificates
 , node-gyp
 , npm
 , node-chalk (>= 2.4.1)
 , node-jsdoc (>= 3.5.5)
 , node-uglify-js (>= 3.3.25)
 , node-espree (>= 3.5.4)
Standards-Version: 4.3.0
Homepage: https://github.com/signalapp/Signal-Desktop.git
Vcs-Git: https://salsa.debian.org/js-team/node-signal-desktop.git
Vcs-Browser: https://salsa.debian.org/js-team/node-signal-desktop

Package: node-signal-desktop
Architecture: all
Depends:
 ${misc:Depends}
 , nodejs (>= 10.15.2~dfsg)
 , node-sqlcipher
 , node-is (>= 0.8.0)
 , backbone (>= 1.3.3)
 , node-blob-util (>= 1.3.0)
 , node-blueimp-canvas-to-blob (>= 3.14.0)
 , node-blueimp-load-image (>= 2.18.0)
 , node-bunyan (>= 1.8.12)
 , node-classnames (>= 2.2.5)
 , node-config (>= 1.28.1)
 , node-electron-editor-context-menu (>= 1.1.1)
 , node-electron-is-dev (>= 0.3.0)
 , node-electron-updater (>= 2.21.10)
 , node-emoji-datasource (>= 4.0.0)
 , node-emoji-datasource-apple (>= 4.0.0)
 , node-emoji-js (>= 3.4.0)
 , node-emoji-panel
 , node-filesize (>= 3.6.1)
 , node-firstline (>= 1.2.1)
 , node-form-data (>= 2.3.2)
 , node-fs-extra (>= 5.0.0)
 , node-glob (>= 7.1.2)
 , node-google-libphonenumber (>= 3.0.7)
 , node-got (>= 8.2.0)
 , node-he (>= 1.2.0)
 , node-intl-tel-input (>= 12.1.15)
 , node-jquery (>= 3.3.1)
 , node-linkify-it (>= 2.0.3)
 , node-lodash (>= 4.17.11)
 , node-mkdirp (>= 0.5.1)
 , node-moment (>= 2.21.0)
 , mustache.js (>= 2.3.0)
 , node-fetch
 , node-gyp (>= 3.8.0)
 , node-os-locale (>= 2.1.0)
 , node-pify (>= 3.0.0)
 , node-protobufjs (>= 6.8.6)
 , node-proxy-agent (>= 3.0.3)
 , node-react (>= 16.2.0)
 , node-react-contextmenu (>= 2.9.2)
 , node-react-dom (>= 16.2.0)
 , node-read-last-lines (>= 1.3.0)
 , node-rimraf (>= 2.6.2)
 , node-sass (>= 4.9.3)
 , node-semver (>= 5.4.1)
 , node-spellchecker (>= 3.4.4)
 , node-tar (>= 4.4.8)
 , node-testcheck (>= 1.0.0-rc.2)
 , node-tmp (>= 0.0.33)
 , node-to-arraybuffer (>= 1.0.1)
 , node-underscore (>= 1.9.0)
 , node-uuid (>= 3.3.2)
 , node-websocket (>= 1.0.28)
 , libnotify4
 , libappindicator1
 , libxtst6
 , libnss3
 , libasound2
 , libxss1
Description: Standalone desktop client for Signal Messenger
 Private messaging from your desktop: signal desktop is a private messaging app
 with built-in end-to-end encrpytion that is always on.
 .
 Signal Desktop is an electron-based app that links with your Signal
 Android or Signal iOS app.
 .
 The Signal Desktop runs independently of your browser.
